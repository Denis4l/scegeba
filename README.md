# SCeGeBa

L'idée est de pouvoir réaliser des scenarii de gestion d'un bassin versant en s'appuyant sur une base de donnée à l'intention des acteurs de l'eau (agriculture, collectivités, industries, loisirs, bureau d'étude, organismes divers).

L'objectif, et ce qui fait la plus value du projet, est de s'appuyer sur une base de données complète, qui permette d'avoir en continue une photographie en temps réél, ou presque, du bassin versant, du point de vue hydrologique.

Il faut donc développer un logiciel, dont le cœur est une base de données. Cette dernière doit pouvoir être renseignée de façon simple par n'importe quel usager de l'eau, depuis n'importe quel type d'ordinateur, dont un téléphone, y compris non SMART phone.

Il y a deux axes de développement :

- l'interface : pratique et opérationnelle. Un agriculteur doit pouvoir utiliser le logiciel depuis son champ sans passer plus de 5 minutes à saisir une donnée;
- la base de données, et son SIG associé, qui permette d'y inclure une foule d'informations sur les rejets, prélèvements, gestionnaires, qualité de l'eau, aspects réglementaires.

Il faut pouvoir répondre à des questions simples comme par exemple :
- Quel volume puis-je prélever cette semaine dans le cours d'eau ?
- Quelle quantité d'eau ai-je apporté à mes tomates hier ?
- Quelles nappes accompagnent ce cours d'eau ?
- etc...

Mais aussi pouvoir gérer des alertes du type : Le cour d'eau est à sec, je renseigne la base de données, et automatiquement cela va informer l'organisme gestionnaire.

Rien d'irréalisable, mais pas mal de boulot quand-même. J'ai besoin d'aide, d'autant que je ne suis pas développeur.

Boulot crucial pour la gestioin de notre environnement, car l'eau est la clé de voute entre lui et notre monde anthropique.
